from pyrogram import Client, Filters
# from pyrogram.errors.exceptions.flood_420 import FloodWait
from time import sleep
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']
a = True


@Client.on_message(Filters.me and Filters.command('buttonclicker', pf))
def buttonclicker(client, message):
    while a:
        message.reply_to_message.click(' '.join(message.command[1:]))
        sleep(0.7)


@Client.on_message(Filters.me and Filters.command('stopclicker', pf))
def stop(client, message):
    global a
    a = False

