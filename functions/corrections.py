from pyrogram import Client, Filters
from time import sleep
import pickle
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.text & Filters.me & Filters.command('correction', pf))
def correction(client, message):
    with open('correction', 'rb') as f:
        corrections = pickle.load(f)
    args = ' '.join(message.command[1:])
    trg, rpl = args.split('-')
    if message.chat.id in corrections:
        corrections[message.chat.id][trg] = rpl[:-1]
    else:
        corrections[message.chat.id] = {trg: rpl[:-1]}
    with open('correction', 'wb') as f:
        pickle.dump(corrections, f)
    message.edit('`Correction added successfully!`')
    sleep(3)
    message.delete()


@Client.on_message(Filters.text & Filters.me & ~Filters.regex('^#.+'))
def correcting(client, message):
    with open('correction', 'rb') as f:
        corrections = pickle.load(f)
    if message.chat.id in corrections:
        for trg, rsp in corrections[message.chat.id].items():
            if trg in message.text:
                message.edit(message.text.replace(trg, rsp), parse_mode='markdown')


@Client.on_message(Filters.text & Filters.outgoing & Filters.command('correctionls', pf))
def list_corrections(client, message):
    with open('correction', 'rb') as f:
        corrections = pickle.load(f)
    ls = []
    try:
        for trg, rsp in corrections[message.chat.id].items():
            ls.append(trg + ' -> ' + rsp)
        if ls:
            message.edit('**Corrections in this Chat**\n▪️ ' + '\n▪️ '.join(ls))
        else:
            message.edit('`No Corrections in this Chat`')
            sleep(3)
            message.delete()
    except KeyError:
        message.edit('`No Corrections in this Chat`')
        sleep(3)
        message.delete()


@Client.on_message(Filters.text & Filters.outgoing & Filters.command('delcorrection', pf))
def del_corrections(client, message):
    with open('correction', 'rb') as f:
        corrections = pickle.load(f)
    trg = ' '.join(message.command[1:])
    try:
        del corrections[message.chat.id][trg[:-1]]
        message.edit('`Deleted Correction!`')
    except KeyError:
        message.edit('`correction not found. Remember, put an extra character at the end`')
    with open('correction', 'wb') as f:
        pickle.dump(corrections, f)
    sleep(3)
    message.delete()
