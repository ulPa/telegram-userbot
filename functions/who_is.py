from pyrogram import Client, Filters
from pyrogram.api import functions
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']

def last_online(user):

    if user.status.recently:
        return "Recently"

    elif user.status.within_week:
        return "Within the last week"

    elif user.status.within_month:
        return "Within the last month"

    elif user.status.long_time_ago:
        return "A long time ago"

    elif user.status.online:
        return "Currently Online"


@Client.on_message(Filters.command('whois', pf) & Filters.me)
def who_is(client, message):
    if message.reply_to_message:
        user = message.reply_to_message.from_user
    else:
        chat = client.get_chat(message.command[1])
        user = client.get_users(chat.id)

    common = client.send(functions.messages.GetCommonChats(user_id=client.resolve_peer(user.id),
                                                           max_id=0,
                                                           limit=0))
    text = '**Who is **[{}](tg://user?id={})**?**\n' \
           '`first name:` {}\n' \
           '`last name:` {}\n' \
           '`user id:` {}\n' \
           '`user name:` @{}\n' \
           '`last seen:`{} \n' \
           '`Bio: ` {}\n' \
           '`profile pictures:` {}\n' \
           '`common chats:` {}'.format(user.first_name, user.id,
                                       user.first_name,
                                       user.last_name,
                                       user.id,
                                       user.username,
                                       last_online(user) if user.status else 'None',
                                       client.get_chat(user.id).description,
                                       client.get_user_profile_photos(user.id).total_count,
                                       len(common.chats))

    message.edit('`sending #whois`')
    try:
        pic = client.download_media(user.photo.big_file_id)
        client.send_photo(message.chat.id,
                          pic,
                          caption=text,
                          parse_mode='markdown')
        message.delete()
    except AttributeError:
        message.edit(text, parse_mode='markdown')
