from pyrogram import Client, Filters
from pyrogram.errors.exceptions.bad_request_400 import MediaCaptionTooLong
from pyrogram.errors.exceptions.bad_request_400 import MessageTooLong
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.me & Filters.command('member_stats', pf) & Filters.group)
def members(client, message):
    admin = 0
    user = 0
    deleted = 0
    bots = 0
    members = 0
    for member in client.iter_chat_members(message.chat.id):
        user += 1
        if member.user.is_deleted:
            deleted += 1
        if member.status == 'administrator' or member.status == 'creator':
            admin += 1
        elif member.status == 'member':
            members += 1
        if member.user.is_bot:
            bots += 1
    message.edit('**Total**: {} \n'
                 'Members: {}\n'
                 'Administrators: {} \n'
                 'Deleted Accounts: {} \n'
                 'Bots: {}'.format(user, members, admin, deleted, bots))


@Client.on_message(Filters.me & Filters.command('admins', pf))
def admins(client, message):
    admins = []
    for member in client.iter_chat_members(message.chat.id):
        if member.status == 'administrator':
            admins.append('[{}](tg://user?id={})'.format(member.user.first_name, member.user.id))
        elif member.status == 'creator':
            admins.append('[{}](tg://user?id={})🔹'.format(member.user.first_name, member.user.id))
    message.edit('\n'.join(admins))


@Client.on_message(Filters.me & Filters.command('members', pf))
def members(client, message):
    member_list = []
    for member in client.iter_chat_members(message.chat.id):
        if member.status == 'administrator':
            member_list.append('[{}](tg://user?id={})🔸'.format(member.user.first_name, member.user.id))
        elif member.status == 'creator':
            member_list.append('[{}](tg://user?id={})🔹'.format(member.user.first_name, member.user.id))
        else:
            member_list.append('[{}](tg://user?id={})'.format(member.user.first_name, member.user.id))
    if len(member_list) < 100:
        message.edit(', '.join(member_list[:100]))
    else:
        message.delete()
        while len(member_list) >= 100:
            message.reply(', '.join(member_list[:100]))
            member_list = member_list[100:]
        message.reply(', '.join(member_list[:100]))


@Client.on_message(Filters.me & Filters.command('getchat', pf))
def getchat(client, message):
    message.edit('`ok`')
    if len(message.command) >= 2:
        chat = client.get_chat(message.command[1])
    else:
        chat = client.get_chat(message.chat.id)
    text = '''**{}**
`Pinned Message:` [{}](t.me/c/{}/{})
`Members:` {}
`id`: {}
`type`: {}
`Description:` 
{}'''
    texttoolong = '''**{}**
    `Pinned Message:` [{}](t.me/c/{}/{})
    `Members:` {}
    `id`: {}
    `type`: {}'''
    try:
        try:
            pic = client.download_media(chat.photo.big_file_id)
            client.send_photo(message.chat.id,
                              pic,
                              text.format(chat.title,
                                          (chat.pinned_message.text if chat.pinned_message.text and len(chat.pinned_message.text) < 100 else 'View Here') if chat.pinned_message else 'None',
                                          str(chat.id).replace('-100', ''),
                                          chat.pinned_message.message_id if chat.pinned_message else message.message_id,
                                          chat.members_count,
                                          chat.id,
                                          chat.type,
                                          chat.description))
            message.delete()
        except MediaCaptionTooLong:
            img = client.send_photo(message.message.chat.id, pic)
            msg = img.reply(texttoolong.format(chat.title,
                                  (chat.pinned_message.text if chat.pinned_message.text and len(chat.pinned_message.text) < 100 else 'View Here') if chat.pinned_message else 'None',
                                  str(chat.id).replace('-100', ''),
                                  chat.pinned_message.message_id if chat.pinned_message else message.message_id,
                                  chat.members_count,
                                  chat.id,
                                  chat.type))
            msg.reply('`Descripion:`\n{}'.format(chat.description))

    except AttributeError:
        try:
            message.edit(text.format(chat.title,
                                          (chat.pinned_message.text if chat.pinned_message.text and len(chat.pinned_message.text) < 100 else 'View Here') if chat.pinned_message else 'None',
                                          str(chat.id).replace('-100', ''),
                                          chat.pinned_message.message_id if chat.pinned_message else message.message_id,
                                          chat.members_count,
                                          chat.id,
                                          chat.type,
                                          chat.description))
        except MessageTooLong:
            message.edit(texttoolong.format(chat.title,
                                          (chat.pinned_message.text if chat.pinned_message.text and len(chat.pinned_message.text) < 100 else 'View Here') if chat.pinned_message else 'None',
                                          str(chat.id).replace('-100', ''),
                                          chat.pinned_message.message_id if chat.pinned_message else message.message_id,
                                          chat.members_count,
                                          chat.id,
                                          chat.type))
            message.reply('`Description:`\n{}'.format(chat.description))

