from pyrogram import Client, Filters
from pyrogram.errors.exceptions.flood_420 import FloodWait
from time import sleep
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']

ON = True


@Client.on_message(Filters.text & Filters.outgoing & Filters.command('spam', pf))
def spam(client, message):
    global ON
    ON = True
    for x in range(1000):
        message.edit(str(x))
        sleep(5)
        try:
            if ON:
                message.reply(
                    '/start Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt '
                    'ut labore '
                    'et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea '
                    'rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum '
                    'dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore '
                    'magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet '
                    'clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, '
                    'consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam '
                    'erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd '
                    'gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in '
                    'hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero '
                    'eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te '
                    'feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh '
                    'euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis '
                    'nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem '
                    'vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat '
                    'nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril '
                    'delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option '
                    'congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, '
                    'consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam '
                    'erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis '
                    'nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit '
                    'esse molestie consequat, vel illum dentee eu feugiat nulla facilisis. At vero eos et accusam et justo duo '
                    'dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. '
                    'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore '
                    'et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea '
                    'rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum '
                    'dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod '
                    'eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no '
                    'rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor '
                    'sit amet, consetetur sadipscibieritr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna '
                    'aliquyam erat. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore '
                    'magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet '
                    'clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, '
                    'consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam '
                    'erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea regit. Stet clita kasd '
                    'gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur '
                    'sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed dia'
                    'm voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea '
                    'takimata sanctus. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor'
                    ' invidunt ut labore et dolore magna aliquyam erat, sed diam')
            else:
                return

        except FloodWait as e:
            message.edit(str(e) + ' waiting ' + str(e.x * 2) + ' secs')
            logger.warn('Flood Error: {} secs'.format(e.x))
            sleep(e.x * 2)


@Client.on_message(Filters.me & Filters.command('stop', pf))
def stop_spam(client, message):
    global ON
    ON = False
    message.edit('`spam stopped`')
    sleep(3)
    message.delete()


@Client.on_message(Filters.text & Filters.outgoing & Filters.regex('^#often-'))
def repeating(client, message):
    messagetext = message.text
    trash, text = messagetext.split('-')
    for x in range(100):
        sleep(1.1)
        message.reply(text)
    message.delete()
