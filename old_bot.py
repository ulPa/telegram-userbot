from pyrogram import Client
from pyrogram import Filters
from pyrogram.api import functions
from pyrogram.errors.exceptions.bad_request_400 import MessageNotModified, MessageTooLong, AboutTooLong
from pyrogram.errors.exceptions.flood_420 import FloodWait
from time import sleep, time, mktime
import pickle
import datetime
import random
from spongemock import spongemock
from zalgo_text import zalgo
import requests
import re

# warns = utils.open_pickle('warns')
me = Client('ulpa_h')
wait = 0.05
with open('trigger', 'rb') as f:
    triggers = pickle.load(f)
with open('correction', 'rb') as f:
    corrections = pickle.load(f)
with open('welcome', 'rb') as f:
    welcomes = pickle.load(f)
with open('warn', 'rb') as f:
    warnings = pickle.load(f)
sht_chats = {}
spamcount = 0
spamwait = 10
spamtime = 0
spamlimit = 100
spamcount = 100
sleeptime = 0
emojis = '😀😃😄😁😆😅😂🤣😇🙂🙃😌😗😜🤪😝😛🤓😎🤩🥳😏😕😭🥶🤯🤬😡😐🥴🤐😵💩👻😈🤝👍👎🤚🧠🐶🐱🐭🐹🐰🦊🐻🐼🐨🐯🦁🐮🐷🐽🐸🐵🙈🙉🙊🐒' \
         '🐔🐗🐞🐢🦐🐬🐑🦃🦜🦔🐿🌹🌷🌳🐲🍀🌙🌎🌪💦💧💨🌈🥨🍕🍟🍔🍪🍯⌛️🦠🧬❤️♣️♥️'


# text manupilation

# --Groups--
# 0 - links; welcome
# 1 - often
# 2 - onecharacter
# 3 - functions with command
# 4 - triggering, correction

def randfloat(start, end):
    return random.random() * (end - start) + start


def german_to_bavarian(text):
    html = requests.post('https://respekt-empire.de/Translator/?page=translateEngine',
                         data={'tr_text': text, 'translate': 'Übersetzen'})
    pos = re.search(r'<div id=\"translatedtext\" class=\"translator\">.+</div>', html.text)

    new_text = html.text[pos.span()[0]:pos.span()[1]][44:-6]
    print(new_text)
    a = new_text.replace('&uuml;', 'ü')
    b = a.replace('&auml;', 'ä')
    c = b.replace('&ouml;', 'ö')
    d = c.replace('&Uuml;', 'Ü')
    e = d.replace('&Auml;', 'Ä')
    f = e.replace('&Ouml;', 'Ö')
    print(f)
    return f


@me.on_message(Filters.text & Filters.outgoing & Filters.regex('^.+\*\d+$'))
def often(client, message):
    if sleeptime < time():
        text = message.text
        text, n = text.split('*')
        sendtext = text * int(n)
        try:
            me.edit_message_text(message.chat.id, message.message_id, text * int(n))
        except MessageTooLong:
            parts = []
            while len(sendtext) > 4096:
                parts.append(sendtext[:4096])
                sendtext = sendtext[4096:]
            me.edit_message_text(message.chat.id, message.message_id, parts[0])
            for part in parts[1:]:
                me.send_message(message.chat.id, part)


# animation


@me.on_message(Filters.text & Filters.outgoing & Filters.regex('^#blink '))
def blink(client, message):
    if sleeptime < time():
        text = message.text[7:]
        for x in range(100):
            me.edit_message_text(message.chat.id, message.message_id, text)
            sleep(1.1)
            me.edit_message_text(message.chat.id, message.message_id, '_' * len(text), 'html')
            sleep(1.1)
        me.edit_message_text(message.chat.id, message.message_id, text)


@me.on_message(Filters.text & Filters.outgoing & Filters.regex('^#slide '))
def slide(c, message):
    if sleeptime < time():
        global wait
        text = message.text[6:]
        for x in range(200):
            for n in range(len(text)):
                new_text = text[n:] + text[:n]
                if new_text != text:
                    try:
                        try:
                            me.edit_message_text(message.chat.id, message.message_id, text[n + 1:] + text[:n + 1])
                            sleep(wait)
                        except MessageNotModified:
                            pass
                    except FloodWait:
                        wait += 0.001


@me.on_message(Filters.command('type', '#') & Filters.me)
def type(client, message):
    if sleeptime < time():
        command = ' '.join(message.command[1:])
        beginning_text = command.split('-')
        texts = beginning_text[1].split('|')
        for char_n in range(len(beginning_text[0])):
            try:
                message.edit(beginning_text[0][:char_n] + '**|**')
                sleep(randfloat(0.02, 0.05))
            except MessageNotModified:
                pass
        for text in texts:
            for x in range(len(text)):
                try:
                    message.edit(beginning_text[0] + text[:x] + '**|**')
                    sleep(randfloat(0.02, 0.5))
                except MessageNotModified:
                    pass
            for a in range(len(text), 0, -1):
                if text != texts[-1]:
                    try:
                        message.edit(beginning_text[0] + text[:a] + '**|**')
                        sleep(randfloat(0.02, 0.5))
                    except MessageNotModified:
                        pass
        message.edit(beginning_text[0] + texts[-1])


# reply to other

@me.on_message(Filters.text & Filters.outgoing & Filters.regex('^#trigger '))
def trigger(client, message):
    if sleeptime < time():
        with open('trigger', 'rb') as f:
            triggers = pickle.load(f)
        args = message.text.replace('#trigger ', '')

        if message.reply_to_message:
            trg = str(message.reply_to_message.text)
            rpl = args
        else:
            try:
                trg, rpl = args.split('-')
            except ValueError:
                message.edit("`Error: one '-' is needed, not more or less.")
        if message.chat.id in triggers:
            triggers[message.chat.id][trg] = rpl
        else:
            triggers[message.chat.id] = {trg: rpl}
        with open('trigger', 'wb') as f:
            pickle.dump(triggers, f)
        message.edit('`Trigger added successfully!`')
        sleep(3)
        message.delete()


@me.on_message(Filters.text & Filters.incoming & ~ Filters.edited)
def triggering(client, message):
    if sleeptime < time():
        global spamcount, spamwait, spamtime, spamlimit, spamcount2
        with open('trigger', 'rb') as f:
            triggers = pickle.load(f)
        if message.chat.id in triggers:
            for trg, rsp in triggers[message.chat.id].items():
                if trg in message.text:
                    if spamcount >= spamlimit:
                        spamwait += 10
                        spamtime = time()
                    elif spamtime >= time() + spamwait:
                        spamcount = 0
                    elif spamtime >= time() + 10000000:
                        spamwait = 0
                    else:
                        try:
                            message.reply(rsp)
                            spamcount += 1
                        except FloodWait:
                            spamlimit -= 1
                            if spamlimit <= 0:
                                spamlimit = spamcount2
                                spamcount2 -= 1
                                if spamcount <= 0:
                                    spamcount2 = 50


@me.on_message(Filters.text & Filters.outgoing & Filters.command('triggerls', '#'))
def list_trigger(client, message):
    if sleeptime < time():
        with open('trigger', 'rb') as f:
            triggers = pickle.load(f)
        ls = []
        try:
            for trg, rsp in triggers[message.chat.id].items():
                ls.append(trg + ' -> ' + rsp)
            if ls:
                message.edit('**Trigger in this Chat**\n▪️ ' + '\n▪️ '.join(ls))
            else:
                message.edit('`No Trigger in this Chat`')
                sleep(3)
                message.delete()
        except KeyError:
            message.edit('`No Trigger in this Chat`')
            sleep(3)
            message.delete()


@me.on_message(Filters.text & Filters.outgoing & Filters.regex('^#deltrigger '))
def del_trigger(client, message):
    if sleeptime < time():
        with open('trigger', 'rb') as f:
            triggers = pickle.load(f)
        trg = message.text.replace('#deltrigger ', '')
        try:
            del triggers[message.chat.id][trg[:-1]]
            with open('trigger', 'wb') as f:
                pickle.dump(triggers, f)
            message.edit('`Deleted Trigger!`')
        except KeyError:
            message.edit('`Trigger not found! Remember, put on an extra character at the end`')
        sleep(3)
        message.delete()


# corrections

@me.on_message(Filters.text & Filters.me & Filters.command('correction', '#'))
def correction(client, message):
    if sleeptime < time():
        with open('correction', 'rb') as f:
            corrections = pickle.load(f)
        args = ' '.join(message.command[1:])
        trg, rpl = args.split('-')
        print(args)
        if message.chat.id in corrections:
            corrections[message.chat.id][trg] = rpl[:-1]
        else:
            corrections[message.chat.id] = {trg: rpl[:-1]}
        with open('correction', 'wb') as f:
            pickle.dump(corrections, f)
        message.edit('`Correction added successfully!`')
        sleep(3)
        message.delete()


@me.on_message(Filters.text & Filters.me & ~Filters.regex('^#.+'))
def correcting(client, message):
    if sleeptime < time():
        with open('correction', 'rb') as f:
            corrections = pickle.load(f)
        if message.chat.id in corrections:
            for trg, rsp in corrections[message.chat.id].items():
                if trg in message.text:
                    message.edit(message.text.replace(trg, rsp), parse_mode='markdown')


@me.on_message(Filters.text & Filters.outgoing & Filters.command('correctionls', '#'))
def list_corrections(client, message):
    if sleeptime < time():
        with open('correction', 'rb') as f:
            corrections = pickle.load(f)
        ls = []
        try:
            for trg, rsp in corrections[message.chat.id].items():
                ls.append(trg + ' -> ' + rsp)
            if ls:
                message.edit('**Corrections in this Chat**\n▪️ ' + '\n▪️ '.join(ls))
            else:
                message.edit('`No Corrections in this Chat`')
                sleep(3)
                message.delete()
        except KeyError:
            message.edit('`No Corrections in this Chat`')
            sleep(3)
            message.delete()


@me.on_message(Filters.text & Filters.outgoing & Filters.command('delcorrection', '#'))
def del_corrections(client, message):
    if sleeptime < time():
        with open('correction', 'rb') as f:
            corrections = pickle.load(f)
        trg = ' '.join(message.command[1:])
        try:
            del corrections[message.chat.id][trg[:-1]]
            message.edit('`Deleted Correction!`')
        except KeyError:
            message.edit('`correction not found. Remember, put an extra character at the end`')
        with open('correction', 'wb') as f:
            pickle.dump(corrections, f)
        sleep(3)
        message.delete()


# welcome message

@me.on_message(Filters.new_chat_members | Filters.command('show_welcome', '#'))
def welcome(client, message):
    if sleeptime < time():
        with open('welcome', 'rb') as f:
            welcomes = pickle.load(f)
        if message.chat.id in welcomes:
            new_members = ", ".join(
                ["[{}](tg://user?id={})".format(i.first_name, i.id) for i in message.new_chat_members])

            text = welcomes[message.chat.id].replace('%name', new_members)

            # Send the welcome message
            client.send_message(message.chat.id,
                                text,
                                reply_to_message_id=message.message_id,
                                disable_web_page_preview=True)


@me.on_message(Filters.me & Filters.command('setwelcome', '#'))
def setwelcome(client, message):
    if sleeptime < time():
        with open('welcome', 'rb') as f:
            welcomes = pickle.load(f)
        welcomes[message.chat.id] = message.text.replace('#setwelcome', '')
        with open('welcome', 'wb') as f:
            pickle.dump(welcomes, f)
        message.edit('`Welcome Message succesfully set.`')
        sleep(3)
        message.delete()


# spam

@me.on_message(Filters.text & Filters.outgoing & Filters.regex('^#spam'))
def spam(client, message):
    if sleeptime < time():
        for x in range(1000):
            message.edit(str(x))
            sleep(10)
            message.reply(
                '/start Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt '
                'ut labore '
                'et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea '
                'rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum '
                'dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore '
                'magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet '
                'clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, '
                'consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam '
                'erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd '
                'gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Duis autem vel eum iriure dolor in '
                'hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero '
                'eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te '
                'feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh '
                'euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis '
                'nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem '
                'vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat '
                'nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril '
                'delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option '
                'congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, '
                'consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam '
                'erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis '
                'nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit '
                'esse molestie consequat, vel illum dolore eu feugiat nulla facilisis. At vero eos et accusam et justo duo '
                'dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. '
                'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore '
                'et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea '
                'rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum '
                'dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod '
                'eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no '
                'rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit amet. Lorem ipsum dolor '
                'sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna '
                'aliquyam erat. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore '
                'magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet '
                'clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, '
                'consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam '
                'erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd '
                'gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur '
                'sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed dia'
                'm voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea '
                'takimata sanctus. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor'
                ' invidunt ut labore et dolore magna aliquyam erat, sed diam')


@me.on_message(Filters.text & Filters.outgoing & Filters.regex('^#often-'))
def repeating(client, message):
    if sleeptime < time():
        messagetext = message.text
        trash, text = messagetext.split('-')
        for x in range(100):
            sleep(1.1)
            message.reply(text)
        message.delete()


# admin
@me.on_message(Filters.text & Filters.outgoing & Filters.reply & Filters.command('ban', '#'))
def ban(client, message):
    if sleeptime < time():
        client.kick_chat_member(message.chat.id, message.reply_to_message.from_user.id)
        message.edit('[{}]({}) is #banned for {}'.format(message.reply_to_message.from_user.first_name,
                                                         'tg://user?id={}'.format(
                                                             message.reply_to_message.from_user.id),
                                                         message.text.replace('#ban ', '')))


@me.on_message(Filters.me & Filters.command('kick', '#'))
def kick(client, message):
    if sleeptime < time():
        client.kick_chat_member(message.chat.id, message.reply_to_message.from_user.id)
        client.unban_chat_member(message.chat.id, message.reply_to_message.from_user.id)
        message.edit('[{}]({}) is #kicked for {}'.format(message.reply_to_message.from_user.first_name,
                                                         'tg://user?id={}'.format(
                                                             message.reply_to_message.from_user.id),
                                                         message.text.replace('#kick ', '')))


@me.on_message(Filters.me & Filters.command('warn', '#'))
def warn(client, message):
    global warnings
    if sleeptime < time():
        rpl_user = message.reply_to_message.from_user
        if message.chat.id in warnings:
            if message.reply_to_message.from_user.id in warnings[message.chat.id]:
                warnings[message.chat.id][message.reply_to_message.from_user.id] += 1
            else:
                warnings[message.chat.id][message.reply_to_message.from_user.id] = 1
        else:
            warnings[message.chat.id] = {message.reply_to_message.from_user.id: 1}
        warns = warnings[message.chat.id][message.reply_to_message.from_user.id]
        if warns == 2:
            now = datetime.datetime.now()
            until = datetime.datetime(now.year, now.month, now.day, now.hour + 1)
            client.restrict_chat_member(message.chat.id,
                                        rpl_user.id,
                                        until_date=int(mktime(until.timetuple()) + 1e-6 * until.microsecond),
                                        can_send_messages=False,
                                        can_change_info=False,
                                        can_invite_users=False,
                                        can_pin_messages=False)
        elif warnings[message.chat.id][message.reply_to_message.from_user.id] == 3:
            client.kick_chat_member(message.chat.id, message.reply_to_message.from_user.id)
            client.unban_chat_member(message.chat.id, message.reply_to_message.from_user.id)
            message.reply('[{}]({}) is #kicked for {}'.format(message.reply_to_message.from_user.first_name,
                                                              'tg://user?id={}'.format(
                                                                  message.reply_to_message.from_user.id),
                                                              'Having 3 Warns'))
        message.edit('[{}]({}) is #warned for {} `{}`'.format(message.reply_to_message.from_user.first_name,
                                                              'tg://user?id={}'.format(
                                                                  message.reply_to_message.from_user.id),
                                                              message.text.replace('#warn ', ''),
                                                              warnings[message.chat.id][
                                                                  message.reply_to_message.from_user.id]))


@me.on_message(Filters.me & Filters.command('restrict', '#', '-'))
def restrict(client, message):
    if sleeptime < time():
        args = message.command
        rpl_user = message.reply_to_message.from_user
        now = datetime.datetime.now()
        reason = args[1]
        days = args[2]
        hours = args[3]
        # TODO: Value Eror day is out of range for month if day is more than days in month.
        until = datetime.datetime(now.year, now.month, now.day + int(days), now.hour + int(hours))
        client.restrict_chat_member(message.chat.id,
                                    rpl_user.id,
                                    until_date=int(mktime(until.timetuple()) + 1e-6 * until.microsecond),
                                    can_send_messages=False,
                                    can_change_info=False,
                                    can_invite_users=False,
                                    can_pin_messages=False)
        message.edit('[{}]({}) is #restricted for {}'.format(message.reply_to_message.from_user.first_name,
                                                             'tg://user?id={}'.format(
                                                                 message.reply_to_message.from_user.id),
                                                             reason))


@me.on_message(Filters.me & Filters.command('members', '#') & Filters.group)
def Members(client, message):
    admin = 0
    user = 0
    deleted = 0
    bots = 0
    members = 0
    for member in client.iter_chat_members(message.chat.id):
        user += 1
        if member.user.is_deleted:
            deleted += 1
        if member.status == 'administrator':
            admin += 1
        elif member.status == 'member':
            members += 1
        if member.user.is_bot:
            bots += 1
    message.edit('Accounts: {} \n'
                 'Members: {}\n'
                 'Administrators: {} \n'
                 'Deleted Accounts: {} \n'
                 'Bots: {}'.format(user, members, admin, deleted, bots))


# other

@me.on_message(Filters.command("poll", "#") & Filters.me)
def make_a_poll(bot, message):
    if sleeptime < time():
        cmd = message.command
        if len(cmd) == 1:
            message.edit("`Not enough arguments`")
        elif len(cmd) > 1:
            poll = message.text[6:].split('\n')
            if len(poll[1:]) < 2:
                message.edit("`Not enough answers`")
            elif len(poll[1:]) > 10:
                message.edit("`Too many answers`")
            else:

                bot.send_poll(
                    chat_id=message.chat.id,
                    question=poll[0],
                    options=poll[1:]
                )
    sleep(3)
    message.delete()




# who is
@me.on_message(Filters.command('whois', '#') & Filters.me)
def who_is(client, message):
    global sleeptime
    if sleeptime < time():
        user = message.reply_to_message.from_user
        common = client.send(functions.messages.GetCommonChats(user_id=client.resolve_peer(user.id),
                                                               max_id=0,
                                                               limit=0))
        text = '**Who is **[{}](tg://user?id={})**?**\n' \
               '`first name:` {}\n' \
               '`last name:` {}\n' \
               '`user id:` {}\n' \
               '`user name:` @{}\n' \
               '`Bio: ` {}\n' \
               '`profile pictures:` {}\n' \
               '`common chats:` {}'.format(user.first_name, user.id,
                                           user.first_name,
                                           user.last_name,
                                           user.id,
                                           user.username,
                                           client.get_chat(user.id).description,
                                           client.get_user_profile_photos(user.id).total_count,
                                           len(common.chats))

        message.edit('`sending #whois`')
        try:
            pic = client.download_media(user.photo.big_file_id)
            client.send_photo(message.chat.id,
                              pic,
                              caption=text,
                              parse_mode='markdown')
            message.delete()
        except AttributeError:
            message.edit(text, parse_mode='markdown')


# update profile'

@me.on_message(Filters.command('name', '#') & Filters.me)
def set_name(client, message):
    global sleeptime
    if sleeptime < time():
        me.send(functions.account.UpdateProfile(first_name=' '.join(message.command[1:])))
        message.edit('`Name changed succesfully`')
        sleep(3)
        message.delete()


@me.on_message(Filters.command('bio', '#') & Filters.me)
def set_bio(client, msg):
    global sleeptime
    if sleeptime < time():
        bio = ' '.join(msg.command[1:])
        if len(bio) > 70:
            msg.edit('`Bio too long maximum 70 characters`')
            sleep(3)
            msg.delete()
        else:
            me.send(functions.account.UpdateProfile(about=bio))
            msg.edit('`Bio succesfully changed`'.format(bio))
            sleep(3)
            msg.delete()


@me.on_message(Filters.regex('^#profilepic') & Filters.me)
def set_profile_pic(client, msg):
    global sleeptime
    if sleeptime < time():
        if msg.reply_to_message:
            pic = client.download_media(msg.reply_to_message.photo)
        else:
            pic = client.download_media(msg.photo)
        client.set_user_profile_photo(pic)
        msg.edit('`profile picture set succesfully`')
        sleep(3)
        msg.delete()


@me.on_message(Filters.text & Filters.regex('^.$'))
def onecharacter(c, message):
    me.send(functions.account.UpdateProfile(last_name='🇩🇪 ' + message.text))


@me.on_message(Filters.command('sleep', '#'))
def sleep_bot(client, message):
    global sleeptime
    # me.stop()
    # sleep(int(message.command[1]))
    secs = int(message.command[1])
    message.edit('`sleeping for {} seconds`'.format(secs))
    sleeptime = time() + secs
    sleep(3)
    message.delete()


# TODO: features from https://github.com/baalajimaestro/Telegram-UserBot/tree/master/userbot/modules


@me.on_message(Filters.text & Filters.me & Filters.command('mock', '#'))
def mock(client, message):
    if sleeptime < time():
        message.edit(spongemock.mock(' '.join(message.command[1:])))


@me.on_message(Filters.text & Filters.command('zal', '#') & Filters.me)
def zal(client, message):
    if sleeptime < time():
        message.edit(zalgo.zalgo().zalgofy(' '.join(message.command[1:])))


@me.on_message(Filters.text & Filters.command('bayr', '#') & Filters.me)
def bayr(client, message):
    if sleeptime < time():
        message.edit(german_to_bavarian(message.text.replace('#bayr ', '')))


@me.on_message(Filters.command('style', '#'))
def sht(client, message):
    """0: nothing
    1: random manipulation of text
    2: all text zalgo
    3: all text mock
    4: all text bavarian"""
    if sleeptime < time():
        global sht_chats
        sht_chats[message.chat.id] = message.command[1]
        message.edit('`set to {}`'.format(message.command[1]))
        sleep(3)
        message.delete()


@me.on_message(Filters.text & Filters.me, group=1)
def text(client, message):
    if sleeptime < time():
        try:
            if sht_chats[message.chat.id] == '1':
                n = random.randint(0, 50)
                if n == 1:
                    message.edit(zalgo.zalgo().zalgofy(message.text))
                elif n == 2:
                    message.edit(spongemock.mock(message.text))
                elif n == 3:
                    message.edit(message.text.replace('B', '🅱'))
                elif n == 4:
                    message.edit(message.text.replace('A', '🅰️'))
                elif n == 5:
                    message.edit(message.text.replace(' ', random.choice(emojis)))
            elif sht_chats[message.chat.id] == '2':
                message.edit(zalgo.zalgo().zalgofy(message.text))
            elif sht_chats[message.chat.id] == '3':
                message.edit(spongemock.mock(message.text))
            elif sht_chats[message.chat.id] == '4':
                text = german_to_bavarian(message.text)
                if text != message.text:
                    message.edit(german_to_bavarian(message.text))
        except KeyError:
            pass


@me.on_message(Filters.text & Filters.outgoing & Filters.regex(r'\w*\[.+\]\(.+\)\w*'), group=2)
def links(client, message):
    if sleeptime < time():
        me.edit_message_text(message.chat.id, message.message_id, message.text, parse_mode='markdown')


me.run()
