from pyrogram import Client, Filters
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.command("r", pf))
def replace(client, message):
    text = message.text.replace('#r ', '')
    fr, to = text.split('/')
    result = "**You mean:**\n{}".format(message.reply_to_message.text.replace(fr, to))
    message.edit(result)
