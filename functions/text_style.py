from pyrogram import Client, Filters
from bot import style_chats
from time import sleep
import requests
import re
from spongemock import spongemock
from zalgo_text import zalgo
import random
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


emojis = '😀😃😄😁😆😅😂🤣😇🙂🙃😌😗😜🤪😝😛🤓😎🤩🥳😏😕😭🥶🤯🤬😡😐🥴🤐😵💩👻😈🤝👍👎🤚🧠🐶🐱🐭🐹🐰🦊🐻🐼🐨🐯🦁🐮🐷🐽🐸🐵🙈🙉🙊🐒' \
         '🐔🐗🐞🐢🦐🐬🐑🦃🦜🦔🐿🌹🌷🌳🐲🍀🌙🌎🌪💦💧💨🌈🥨🍕🍟🍔🍪🍯⌛️🦠🧬❤️♣️♥️'


def german_to_bavarian(text):
    html = requests.post('https://respekt-empire.de/Translator/?page=translateEngine',
                         data={'tr_text': text, 'translate': 'Übersetzen'})
    pos = re.search(r'<div id=\"translatedtext\" class=\"translator\">.+</div>', html.text)

    new_text = html.text[pos.span()[0]:pos.span()[1]][44:-6]
    a = new_text.replace('&uuml;', 'ü')
    b = a.replace('&auml;', 'ä')
    c = b.replace('&ouml;', 'ö')
    d = c.replace('&Uuml;', 'Ü')
    e = d.replace('&Auml;', 'Ä')
    f = e.replace('&Ouml;', 'Ö')
    return f


def strikethrough(text: str):
    SPEC = '̶'
    return ''.join([x + SPEC if x != ' ' else ' ' for x in text])


@Client.on_message(Filters.text & Filters.me & Filters.command('mock', pf))
def mock(client, message):
    message.edit(spongemock.mock(' '.join(message.command[1:])))


@Client.on_message(Filters.text & Filters.command('zal', pf) & Filters.me)
def zal(client, message):
    message.edit(zalgo.zalgo().zalgofy(' '.join(message.command[1:])))


@Client.on_message(Filters.text & Filters.command('bayr', pf) & Filters.me)
def bayr(client, message):
    message.edit(german_to_bavarian(message.text.replace('#bayr ', '')))


@Client.on_message(Filters.command('style', pf))
def style(client, message):
    """0: nothing
    1: random manipulation of text
    2: all text zalgo
    3: all text mock
    4: all text bavarian"""
    style_chats[message.chat.id] = message.command[1]
    message.edit('`set to {}`'.format(message.command[1]))
    sleep(3)
    message.delete()


@Client.on_message(Filters.text & Filters.me, group=1)
def text(client, message):
    try:
        if style_chats[message.chat.id] == '1':
            n = random.randint(0, 50)
            if n == 1:
                message.edit(zalgo.zalgo().zalgofy(message.text))
            elif n == 2:
                message.edit(spongemock.mock(message.text))
            elif n == 3:
                message.edit(message.text.replace('B', '🅱'))
            elif n == 4:
                message.edit(message.text.replace('A', '🅰️'))
            elif n == 5:
                message.edit(message.text.replace(' ', random.choice(emojis)))
        elif style_chats[message.chat.id] == '2':
            message.edit(zalgo.zalgo().zalgofy(message.text))
        elif style_chats[message.chat.id] == '3':
            message.edit(spongemock.mock(message.text))
        elif style_chats[message.chat.id] == '4':
            text = german_to_bavarian(message.text)
            if text != message.text:
                message.edit(german_to_bavarian(message.text))
    except KeyError:
        pass
