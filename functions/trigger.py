from pyrogram import Client, Filters
from time import time, sleep
from pyrogram.errors.exceptions.flood_420 import FloodWait
from bot import sleeptime, spamcount, spamlimit, spamtime, spamwait
import pickle
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.text & Filters.outgoing & Filters.command('trigger', pf))
def trigger(client, message):
    with open('trigger', 'rb') as f:
        triggers = pickle.load(f)
    args = message.text.replace('#trigger ', '')
    if message.reply_to_message:
        trg = str(message.reply_to_message.text)
        rpl = args
    else:
        try:
            trg, rpl = args.split('-')
        except ValueError:
            message.edit("`Error: one '-' is needed, not more or less.")
    if message.chat.id in triggers:
        triggers[message.chat.id][trg] = rpl
    else:
        triggers[message.chat.id] = {trg: rpl}
    with open('trigger', 'wb') as f:
        pickle.dump(triggers, f)
    message.edit('`Trigger added successfully!`')
    sleep(3)
    message.delete()


@Client.on_message(Filters.text & Filters.incoming & ~ Filters.edited, group=3)
def triggering(client, message):
    global spamcount, spamwait, spamtime, spamlimit, spamcount2
    with open('trigger', 'rb') as f:
        triggers = pickle.load(f)
    if message.chat.id in triggers:
        for trg, rsp in triggers[message.chat.id].items():
            if trg in message.text:
                if spamcount >= spamlimit:
                    spamwait += 10
                    spamtime = time()
                elif spamtime >= time() + spamwait:
                    spamcount = 0
                elif spamtime >= time() + 10000000:
                    spamwait = 0
                else:
                    try:
                        message.reply(rsp)
                        spamcount += 1
                    except FloodWait:
                        spamlimit -= 1
                        if spamlimit <= 0:
                            spamlimit = spamcount2
                            spamcount2 -= 1
                            if spamcount <= 0:
                                spamcount2 = 50


@Client.on_message(Filters.text & Filters.outgoing & Filters.command('triggerls', pf))
def list_trigger(client, message):
    with open('trigger', 'rb') as f:
        triggers = pickle.load(f)
    ls = []
    try:
        for trg, rsp in triggers[message.chat.id].items():
            ls.append(trg + ' -> ' + rsp)
        if ls:
            message.edit('**Trigger in this Chat**\n▪️ ' + '\n▪️ '.join(ls))
        else:
            message.edit('`No Trigger in this Chat`')
            sleep(3)
            message.delete()
    except KeyError:
        message.edit('`No Trigger in this Chat`')
        sleep(3)
        message.delete()


@Client.on_message(Filters.text & Filters.outgoing & Filters.regex('^#deltrigger '))
def del_trigger(client, message):
    with open('trigger', 'rb') as f:
        triggers = pickle.load(f)
    trg = message.text.replace('#deltrigger ', '')
    try:
        del triggers[message.chat.id][trg[:-1]]
        with open('trigger', 'wb') as f:
            pickle.dump(triggers, f)
        message.edit('`Deleted Trigger!`')
    except KeyError:
        message.edit('`Trigger not found! Remember, put on an extra character at the end`')
    sleep(3)
    message.delete()

