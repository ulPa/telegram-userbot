# MIT License
#
# Copyright (c) 2018 Dan Tès <https://github.com/delivrance>
# Copyright (c) 2019 ulPa <https://gitlab.com/ulPa> (edited)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import requests
from configparser import ConfigParser
from pyrogram import Client, Filters
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


BASE = "https://haste.thevillage.chat"


@Client.on_message(Filters.command("haste", pf) & Filters.me)
def haste(client, message):
    message.edit('`ok`')
    reply = message.reply_to_message
    if reply:
        text = reply.text
    else:
        text = message.text.replace('#haste ', '')

    if not text:
        return

    result = requests.post("{}/documents".format(BASE), data=text.encode("UTF-8")).json()

    message.edit("{}/{}".format(BASE, result["key"]))
