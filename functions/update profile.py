from pyrogram import Client, Filters
from time import sleep
from pyrogram.api import functions
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.command('name', pf) & Filters.me)
def set_name(client, message):
    client.send(functions.account.UpdateProfile(first_name=' '.join(message.command[1:])))
    message.edit('`Name changed succesfully`')
    sleep(3)
    message.delete()


@Client.on_message(Filters.command('bio', pf) & Filters.me)
def set_bio(client, msg):
    bio = ' '.join(msg.command[1:])
    if len(bio) > 70:
        msg.edit('`Bio too long maximum 70 characters`')
        sleep(3)
        msg.delete()
    else:
        client.send(functions.account.UpdateProfile(about=bio))
        msg.edit('`Bio succesfully changed`'.format(bio))
        sleep(3)
        msg.delete()


@Client.on_message(Filters.regex('^#profilepic') & Filters.me)
def set_profile_pic(client, msg):
    if msg.reply_to_message:
        pic = client.download_media(msg.reply_to_message.photo)
    else:
        pic = client.download_media(msg.photo)
    client.set_user_profile_photo(pic)
    msg.edit('`profile picture set succesfully`')
    sleep(3)
    msg.delete()


@Client.on_message(Filters.text & Filters.regex('^.$'))
def onecharacter(client, message):
    client.send(functions.account.UpdateProfile(last_name='🇩🇪 ' + message.text))
